#!/bin/bash

./liquibase --username=admin --password=admin123 --changelog-file=example-changelog.xml status
./liquibase  --changelog-file=example-changelog.xml update-sql
./liquibase  --changelog-file=example-changelog.xml update
